/*
        Copyright 2021 Ian Tester

        This file is part of dump_sf7.

        dump_sf7 is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        dump_sf7 is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with dump_sf7.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <vector>
#include <string>
#include <unordered_map>
#include <stdint.h>

namespace Sega {

  namespace SF7000 {

    // Size of each disk (each side is read seperately)
    const int tracks_per_disk = 40;
    const int sectors_per_track = 16;
    const int sector_size = 256;

    // Derived sizes
    const int track_size = sector_size * sectors_per_track;
    const int disk_size = track_size * tracks_per_disk;

    // 'Clusters' is used by the file system
    const int sectors_per_cluster = 4;
    const int cluster_size = sector_size * sectors_per_cluster;

    //! Map from File::type value to a text description
    const std::string file_type_names[3] = {
      "non-ASCII",
      "ASCII",
      "hexadecimal",
    };


    class Disk;

    //! Class representing a file on a disk
    class File {
    public:
      //! File types
      enum class type : uint8_t {
	non_ascii		= 0,
	ascii		= 1,
	hexadecimal	= 2,
      };

      //! Raw file name
      std::string raw_filename(void) const;

      //! File name
      std::string filename(void) const;

      //! File type
      type filetype(void) const;

      //! Is the file read-only?
      bool readonly(void) const;

      //! Read contents
      const std::vector<uint8_t> read(void);

    private:
      std::string _raw_filename, _filename;
      uint8_t _first_cluster;
      type _filetype;
      bool _readonly;
      const Disk *_disk;

      File(std::string rfn, std::string fn, uint8_t fc, uint8_t attr, const Disk* d);

      enum _file_attribute_flags : uint8_t {
	FILE_ATTR_RO		= 0x80,
	FILE_ATTR_TYPE_MASK	= 0x0f,
      };

      // Low-level functions
      void _read_cluster(uint8_t cnum, std::vector<uint8_t>& dest) const;
      uint8_t _fat_entry(uint8_t cnum) const;

      friend class Disk;

    };


    //! Class representing a disk
    class Disk {
    private:
      enum class _structure {
	id_start		= 0,
	id_size			= 4,

	name_start		= 4,
	name_size		= 28,

	IPL_start		= 32,
	IPL_size		= sector_size - 32,

	_reserved_0_start	= sector_size,
	_reserved_0_size	= 15 * sector_size,

	system_programs_start	= track_size,
	system_programs_size	= 19 * track_size,

	directory_start		= 20 * track_size,
	directory_size		= 12 * sector_size,

	FAT_start		= (20 * track_size) + (12 * sector_size),
	FAT_size		= sector_size,

	user_start		= 21 * track_size,
	user_size		= 19 * track_size,
      };

      struct _dir_entry {
	const char filename[12];	// 8.3, including the period
	uint8_t first_cluster;
	uint8_t attribute;
	uint8_t _unused[2];
      };

      const int dir_entry_size = sizeof(_dir_entry);
      const int max_dir_entries = static_cast<int>(_structure::directory_size) / dir_entry_size;

      enum class _fat_entry_flags {
	LAST_CLUSTER_PREFIX		= 0xc0,
	LAST_CLUSTER_MASK		= 0xf0,
	LAST_CLUSTER_NUM_SECTORS_MASK	= 0x0f,

	RESERVED			= 0xfe,
	UNUSED				= 0xff,
      };

      std::vector<uint8_t> _data;

      // Low-level functions
      void _read_sector(uint16_t snum, std::vector<uint8_t>& dest) const;

      friend class File;

    public:
      //! Empty constructor
      Disk();

      //! Load data from a buffer
      void load_data(const uint8_t* data, uint16_t length);

      //    uint32_t id(void) const;

      //! Is this a system disk?
      bool is_sys(void) const;

      //! Disk name
      const std::string name(void) const;

      //! Initial Program Loader
      const std::vector<uint8_t> IPL(void) const;

      //! List the files on disk
      const std::vector<File> list_directory(std::unordered_map<uint8_t, std::string>& charmap) const;

    }; // class Disk


  }; // namespace SF7000

}; // namespace Sega
